
/* Controllers */

angular.module('myApp.controllers', []).
  controller('AppCtrl', function ($scope, socket) {
    socket.on('send:name', function (data) {
      $scope.name = data.name;
    });
  }).
  controller('MyCtrl1', function ($scope, socket) {
    socket.on('send:time', function (data) {
      $scope.time = data.time;
    });
  }).
  controller('MyCtrl2', function ($scope, socket, $filter) {
      $scope.aybc = {
        name: {
          1:"Player 1",
          2:"Player 2",
          3:"Player 3",
          4:"Player 4",
          5:"Player 5"
        }
      };
      $scope.wrc = {
        name: {
          1:"Player 6",
          2:"Player 7",
          3:"Player 8",
          4:"Player 9",
          5:"Player 10"
        }
      };
    socket.on('send:time', function (data) {
      $scope.time = data.time;

      $scope.cs1 = (new Date(2015,03,29,13,30) - new Date());
      $scope.cs2 = (new Date(2015,03,29,14,30) - new Date());
      $scope.dota1 = (new Date(2015,03,29,13,45) - new Date());
      $scope.dota2 = (new Date(2015,03,29,14,45) - new Date());
      $scope.lol1 = (new Date(2015,03,29,12,00) - new Date());
      $scope.lol2 = (new Date(2015,03,29,13,00) - new Date());
    });
  });
